<?php

namespace App\Controller;

use App\Entity\Type;
use App\Form\TypeType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TypeController extends AbstractController
{
    #[Route('/type/list', name: 'app_type_list')]
    public function list(ManagerRegistry $doctrine)
    {
        $types = $doctrine
            ->getRepository(Type::class)
            ->findAll();

        return $this->render(
            'type/list.html.twig',
            ['types' => $types]
        );
    }

    #[Route('/type/save/{id}', name: 'app_type_save')]
    public function update(Request $request, ManagerRegistry $doctrine, ?Type $type = null, int $id = null)
    {
        if (!$type) {
            $type = new Type();
        }

        $form = $this->createForm(TypeType::class, $type);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $doctrine->getManager();
            $entityManager->persist($type);
            $entityManager->flush();
            $this->addFlash('success', 'Le type a été sauvegardé');

            return $this->redirectToRoute('app_type_list');
        }


        return $this->render('type/form.html.twig', array(
            'form' => $form->createView(),
            'type' => $type
        ));
    }

    #[Route('/type/delete/{id}', name: 'app_type_delete')]
    public function delete(ManagerRegistry $doctrine, ?Type $type)
    {
        $entityManager = $doctrine->getManager();
        if (!$type) {
            throw $this->createNotFoundException(
                'Aucun type trouvé'
            );
        }

        $entityManager->remove($type);
        $entityManager->flush();
        $this->addFlash('success', 'Le type a été supprimé');

        return $this->redirectToRoute('app_type_list');
    }
}
