<?php

namespace App\Controller;

use App\Entity\Ingredient;
use App\Form\IngredientType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IngredientController extends AbstractController
{
    #[Route("/ingredient/list", name: "app_ingredient_list")]
    public function list(ManagerRegistry $doctrine): Response
    {
        $ingredients = $doctrine
            ->getRepository(Ingredient::class)
            ->findAllOrderedByName();

        return $this->render(
            'ingredient/list.html.twig',
            ['ingredients' => $ingredients]
        );
    }

    #[Route("ingredient/create", name: "app_ingredient_create")]
    public function create(Request $request, ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();
        $ingredient = new Ingredient();
        $form = $this->createForm(IngredientType::class, $ingredient);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($ingredient);
            $entityManager->flush();
            $this->addFlash('success', 'L\'ingrédient a été créé');

            return $this->redirectToRoute('app_ingredient_list');
        }

        return $this->render('ingredient/create.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    #[Route("/ingredient/update/{id}", name: "app_ingredient_update")]
    public function update(Request $request, ManagerRegistry $doctrine, int $id): Response
    {
        /** @var Ingredient $ingredient */
        $ingredient = $doctrine
            ->getRepository(Ingredient::class)
            ->find($id);
        if (!$ingredient) {
            throw $this->createNotFoundException(
                'Aucun ingrédient trouvé pour l\'id '.$id
            );
        }

        $form = $this->createForm(IngredientType::class, $ingredient);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $doctrine->getManager();
            $entityManager->flush();
            $this->addFlash('success', 'L\'ingrédient a été mis à jour');

            return $this->redirectToRoute('app_ingredient_list');
        }


        return $this->render('ingredient/update.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    #[Route("/ingredient/delete/{id}", name: "app_ingredient_delete")]
    public function delete(ManagerRegistry $doctrine, int $id)
    {
        $entityManager = $doctrine->getManager();
        /** @var Ingredient $ingredient */
        $ingredient = $doctrine
            ->getRepository(Ingredient::class)
            ->find($id);
        if (!$ingredient) {
            throw $this->createNotFoundException(
                'Aucun ingrédient trouvé pour l\'id '.$id
            );
        }

        $entityManager = $doctrine->getManager();
        $entityManager->remove($ingredient);
        $entityManager->flush();

        $this->addFlash('success', 'L\'ingrédient a été supprimé');

        return $this->redirectToRoute('app_ingredient_list');
    }
}
