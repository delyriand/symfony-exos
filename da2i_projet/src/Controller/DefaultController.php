<?php
namespace App\Controller;

use App\Service\ApplicationConfig;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    #[Route('/', name: 'app_homepage')]
    public function indexAction()
    {
        return $this->render('index.html.twig');
    }

    #[Route('/hello/{name}', name: 'app_hello')]
    public function helloAction(ApplicationConfig $applicationConfig, $name = 'Inconnu')
    {
        return $this->render('hello.html.twig', [
            'site_name' => $applicationConfig->getSiteName(),
            'name' => ucfirst($name),
        ]);
    }
}
