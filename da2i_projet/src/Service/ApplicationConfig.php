<?php
namespace App\Service;

use Psr\Log\LoggerInterface;

class ApplicationConfig
{
    public function __construct(
        private LoggerInterface $logger,
        private string $siteName
    ) {
    }

    public function getSiteName(): string
    {
        $this->logger->info('Call method: '.__FUNCTION__);

        return $this->siteName;
    }
}
