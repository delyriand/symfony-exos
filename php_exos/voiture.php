<?php

abstract class Voiture
{
    public float $prix;
    public string $marque;

    public function __construct(string $marque, float $prix)
    {
        $this->prix = $prix;
        $this->marque = $marque;
    }
}

class Renault extends Voiture
{
    public function __construct(float $prix)
    {
        parent::__construct('Renault', $prix);
    }
}

class Peugeot extends Voiture
{
    public function __construct(float $prix)
    {
        parent::__construct('Peugeot', $prix);
    }
}

class BMW extends Voiture
{
    public function __construct(float $prix)
    {
        parent::__construct('BMW', $prix);
    }
}

class Audi extends Voiture
{
    public function __construct(float $prix)
    {
        parent::__construct('Audi', $prix);
    }
}

$voitures = [
    new Renault(30000),
    new Peugeot(27500),
    new Renault(17000),
    new BMW(44300),
    new Audi(37000),
];
?>

<table>
    <tr>
        <th>Marque</th>
        <th>Prix</th>
    </tr>
    <?php foreach ($voitures as $voiture) : ?>
        <tr>
            <td><?php echo $voiture->marque ?></td>
            <td><?= $voiture->prix ?></td>
        </tr>
    <?php endforeach; ?>
</table>
